import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Registro } from '../model/registro.model';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

/**
 * Clase service que se comunica con el back-end en srpring
 */
@Injectable({
  providedIn: 'root'
})
export class RegistroService {
  // Se declara el punto de acceso hacia el back-end
  private urlEndPoint: string = 'http://localhost:8080/api';
  constructor(private http: HttpClient) { }

  /**
   * Método que llama al back para solicitar la lista de registros en el sistema
   */
  getRegistros(): Observable<Registro[]> {
    console.log('ClienteService: tap 1');
    return this.http.get<Registro[]>(this.urlEndPoint + '/listar');
  }

  /**
   * Método que llama al back-end para persistir un registro
   * Si todo sale ok regresa el mismo registro + el id
   */
  create(registro: Registro):Observable<Registro> {
    return this.http.post(this.urlEndPoint + '/registro', registro)
    .pipe(
      map((response:any) => response.registro as Registro),
      catchError(e=>{
        if (e.status === 400) {
          return throwError(e);
        }

        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }

        return throwError(e);
      })
    );
  }

  /**
   * Método que envía al back, un arreglo con los ids d los registros a ser persistidos
   */
  update(registros: Registro[]): Observable<any> {
    return this.http.put<any>(`${this.urlEndPoint}/update`, registros).pipe(
      catchError(e => {

        if (e.status === 400) {
          return throwError(e);
        }

        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }

        return throwError(e);
      })
    );
  }
}
