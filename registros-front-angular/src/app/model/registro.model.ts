export class Registro {
  id: number;
  nombre: string;
  apellido: string;
  procesado: boolean;
}
