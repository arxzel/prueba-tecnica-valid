import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Registro } from 'src/app/model/registro.model';
import { RegistroService } from 'src/app/service/registro.service';
import { FormControl } from '@angular/forms';

// para usar jquery dentro de TypeScript
// es necesario declarar esta variable global
declare var $:any;

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  // Se crean los elementos a utilizar en la clase
  registros: Registro[];
  errores: string[];
  autoCompleteControl = new FormControl();

  // Se inyectan los elementos necesarios
  constructor(private registroService: RegistroService,
    private router:Router) {
      this.router.routeReuseStrategy.shouldReuseRoute = () => {
        return false;
      };
    }

    // Una vez se inicia la vista se cargan los elementos en la misma
  ngOnInit() {
    this.registroService.getRegistros().subscribe(registros => this.registros = registros);
  }

  /**
   * Método  para cambiar el estado de proceso de un registro
   */
  public procesar(registro: Registro){
    // Se pide que confirme la operación
    if(!confirm("Realmente desea cambiar el estado del registro: "+registro.nombre + " " + registro.apellido)) return;

    // Se crea un array y luego se añade el id del objeto registro seleccionado
    var select = new Array();
    select.push(registro.id);

    // se llama al método cambiar que ejecuta la operación a través del servicio
    this.cambiar(select);

  }

  /***
   * Método para cambiar el estado de varios registros al usar el check box
   */
  public cambiarEstados(){
    // Se crea un array y se pobla con los ids de los registros seleccionados en el front
    var selectedList = new Array();
    $("input.cambiar:checked").each(function() {
      selectedList.push($(this).val());
    });

    // Si el array es vacio notifica al usuario que debe seleccionar al menos un elemento
    if(selectedList.length==0){
      alert("Sebe seleccionar un registro al menos");
      return;
    }

    // Se pide al usuario que confirme la operación y se llama al método cambiar que ejecuta
    // la operación a través del servicio
    if(!confirm("¿Realmente desea cambiar el estado de los registros seleccionados?")) return;
    console.log(selectedList);
    this.cambiar(selectedList);
  }

  /**
   * Este método recibe como parámetro un array con los ids de los elementos a cambiar
   * en su attributo procesado
   */
  private cambiar(SlectedList){
    // Se llama al servicio y se ejecuta la opción UPDATE
    this.registroService.update(SlectedList).subscribe(
      // La respuesta recibida en formato JSON permite recorrer el mensaje del map que viene desde spring
      json=>{
        // Se muestra el mensaje y luego se recarga la página
        alert(`${json.mensaje}`);
        const currentRoute = this.router.url;
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigate([currentRoute]); // navigate to same route
        });

      },err=>{
        // Si hay algún error se muestran los errores
        this.errores = err.error.errors as string[];
        console.error('Código del error desde el backend: ' + err.status);
        console.error(err.error.errors);
      }
    );
  }

}
