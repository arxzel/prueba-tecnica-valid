import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Registro } from 'src/app/model/registro.model';
import { RegistroService } from 'src/app/service/registro.service';

@Component({
  selector: 'app-registros',
  templateUrl: './registros.component.html',
  styleUrls: ['./registros.component.css']
})
export class RegistrosComponent implements OnInit {

  // Se crean en instancian los objetos de la clase
  public registro: Registro = new Registro();
  errores: string[];

  // Se construye lo necesario con la inyección de dependencias
  constructor(private registroService: RegistroService,
    private router: Router) { }

  ngOnInit() {
  }

  /**
   * Llama al servicio para crear un objeto en el back-end
  */
  public addRegistro():void {
    // Se subscribe al evento para obtener el objeto de regreso
    this.registroService.create(this.registro).subscribe(
      registro=>{
        // Si todo sale ok, redirecciona a listar y muestra el mensaje de éxito
        this.router.navigate(['/listar']);
        alert('El nuevo registro ' + registro.nombre + ' ' + registro.apellido + ' fué creado con éxito');
      }, err => {
        // Si ocurrió un error durante el flujo del proceso se muestran los errores
        this.errores = err.error.errors as string[];
        console.error('Código del error desde el backend: ' + err.status);
        console.error(err.error.errors);
      }
    )
  }

}
