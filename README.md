# Prueba Tecnica Valid

Prueba técnica para Valid en JAVA

Prueba Práctica - Backend Developer
 
## Instrucciones

- Abrir en Spring STS el proyecto maven "PruebaPractica"
- Ejecutar como spring boot APP 
- Abrir el navegador en la URL http://localhost:8080

### Consideraciones si quiere modificar el front

- Si se quiere modificar el front debe trabajarlo como un proyecto en angular (registros-front-angular)
- Una vez hechas las modificaciones y generar los ficheros de producción (ng build --prod) copiar el contenido de dist/registros-front-angular/ al proyecto spring en el directorio src/main/resources/public de spring y ejecutar de nuevo la APP en spring.


#### [Por favor leer el archivo IMPORTANTE LEER.TXT ](https://gitlab.com/arxzel/prueba-tecnica-valid/-/blob/master/IMPORTANTE%20LEER.txt)


