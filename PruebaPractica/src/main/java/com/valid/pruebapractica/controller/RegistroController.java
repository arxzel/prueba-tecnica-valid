package com.valid.pruebapractica.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.valid.pruebapractica.entity.Registro;
import com.valid.pruebapractica.service.IRegistroService;

/**
 * 
 * Clase que funciona como controlador tipo REST y APi para el front en Angular
 * 
 * @author Fabián Millán
 * 
 */

@CrossOrigin(origins = { "http://localhost:4200", "*" })
@RestController
@RequestMapping("/api")
public class RegistroController {

	// Se inyecta el servicio de transacciones CRUD
	@Autowired
	private IRegistroService registroService;

	/**
	 * Método GET que retorna una lista de todos los registros hacia el front
	 * 
	 * @return Lista de objetos tipo Registro
	 * @see {@link Registro}
	 */
	@GetMapping(value = { "", "/", "/listar" })
	public List<Registro> index() {
		return registroService.findAll();
	}

	/**
	 * Método POST que retorna un ResponseEntity genérico que es traspolado a JSON
	 * en el front. recibe un objeto de tipo registro para ser almacenado y
	 * persistido en la DB.
	 * 
	 * @return Un mensaje de éxito o error con la información necesaria para el
	 *         FRONT
	 * @param registro un objeto de tipo {@link Registro}
	 * @param result   el resultado de la traspolación del json al objeto y estado
	 *                 de la petición
	 * @see {@link Registro}
	 */
	@PostMapping("/registro")
	public ResponseEntity<?> create(@Valid @RequestBody Registro registro, BindingResult result) {
		// Creamos el objeto de respuesta hacia el front
		// La lista de errores que pueden surgir
		// Y un objeto registro que se insertará en la base de datos
		Map<String, Object> response = new HashMap<>();
		List<String> errors = new ArrayList<String>();
		Registro registroNuevo = null;

		// Si hay algún error en el bindeo, mapeo o casteo del objeto
		// recibido a la entity se generan los mensajes de error para notificar
		if (result.hasErrors()) {
			System.err.println("Errores encontrados en la validación");
			errors = result.getFieldErrors().stream()
					.map(err -> "El campo '" + err.getField() + "':" + err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("mensaje", "Error al crear la entidad del registro");
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			// Se guarda el registro en la base de datos
			registroNuevo = registroService.save(registro);
		} catch (DataAccessException e) {
			// Si ocurre un error durante la transacción se notifica al front
			errors.add(e.getMostSpecificCause().getMessage());
			response.put("mensaje", "Error en la inserción en la base de datos");
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NullPointerException e) {
			// Igual si hay un valor que no debe ir nulo se notifica
			errors.add(e.getMessage());
			response.put("mensaje", "Error en valor nulo");
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// Si todo sale bien se notifica con un mensaje de éxito y una respuesta
		// positiva HTTP
		response.put("mensaje", "El registro ha sido creado con éxito");
		response.put("registro", registroNuevo);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	/**
	 * Método PUT que retorna un ResponseEntity genérico que es traspolado a JSON en
	 * el front. recibe un array con todos los IDS de los registros para ser
	 * modificados en su atribto procesado.
	 * 
	 * Recibe un array de los ids ({@link Registro#getId()}) y cambia de estado el
	 * atributo procesado ({@link Registro#isProcesado()})
	 * 
	 * @return Un mensaje de éxito o error con la información necesaria para el
	 *         FRONT
	 * @param idsRegistros un array con los ids de los registros a modificar
	 * @param result       El result de la traspolación de los objetos y la petición
	 * @see {@link Registro} {@link Registro#isProcesado()} {@link Registro#getId()}
	 */
	@PutMapping("/update")
	public ResponseEntity<?> updateProcesado(@Valid @RequestBody Long[] idsRegistros, BindingResult result) {
		// Se genera la respuesta de tipo clave-valor
		Map<String, Object> response = new HashMap<>();

		// Se recorre cada id de los registros para obtener el REgistro asociado
		for (Long id : idsRegistros) {
			// Se busca el Registro por ID
			Registro registroActual = registroService.findById(id);

			// Si hay un arror en la traspilación o bindeo del objeto se generan
			// los mensajes de error y se regresa a la vista
			if (result.hasErrors()) {
				List<String> errors = result.getFieldErrors().stream()
						.map(err -> "El campo '" + err.getField() + "':" + err.getDefaultMessage())
						.collect(Collectors.toList());
				response.put("mensaje", "Error al la inserción en la base de datos");
				response.put("errors", errors);
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			}

			// Si el registro no existiere, se muestra el mensaje en la vista
			if (registroActual == null) {
				response.put("mensaje", "El registro con ID: ".concat(id.toString()).concat(" no ha sido encontrado"));
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
			}

			try {
				// Se cambia su atributo procesado según correspona al inverso
				// y luego se guarda en la DB
				registroActual.setProcesado(!registroActual.isProcesado());
				registroActual = registroService.save(registroActual);
			} catch (DataAccessException e) {
				// Si hay un error durante el proceso, se interrumpe y se regresa el mensaje de
				// error
				response.put("mensaje", "Error al la actualización en la base de datos");
				response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		// Si el proceso a ha sido exitoso, se envía una respuesta positiva
		response.put("mensaje", "Los registros han sido actualizados con éxito");
		response.put("success", "OK");

		// se Retorna una response con un estatus de creeado hacia el observador
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
}
