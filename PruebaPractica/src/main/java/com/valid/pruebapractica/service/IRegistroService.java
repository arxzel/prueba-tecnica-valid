package com.valid.pruebapractica.service;

import java.util.List;

import com.valid.pruebapractica.dao.IRegistroDao;
import com.valid.pruebapractica.entity.Registro;

/**
 * Interface que será usada en los controladores como cortafuegos hacia el
 * repository Esta actuara como servicio guardando una instancia de
 * {@link IRegistroServiceImp} que hará las transacciones a la DB a través del
 * DAO {@link IRegistroDao}
 * 
 * @see {@link IRegistroServiceImp} {@link IRegistroDao}
 * @author Fabián Millán
 */
public interface IRegistroService {

	/**
	 * Regresa todos los registros guardados en la database
	 * 
	 * @return una lista con todos los registros en la DB
	 * @see {@link Registro}
	 */
	List<Registro> findAll();

	/**
	 * Guarda un registro en la DB y lo regresa (Util para obtener el id)
	 * 
	 * @return el registro guardado
	 * @param registro el registro a persistir
	 * @see {@link Registro}
	 */
	Registro save(Registro registro);

	/**
	 * Recibe un objeto de tipo Registro y es almacenado en la DB
	 * 
	 * @param registro objeto de tipo Registro a ser actualizado
	 * @see {@link Registro}
	 */
	void update(Registro registro);

	/**
	 * Busca un objeto en la DB y lo regresa por su ID
	 * 
	 * @see {@link Registro}
	 * @param id el id único del objeto a buscar en la DB
	 */
	Registro findById(Long id);
}
