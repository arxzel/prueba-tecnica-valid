package com.valid.pruebapractica.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.valid.pruebapractica.dao.IRegistroDao;
import com.valid.pruebapractica.entity.Registro;

/**
 * Clase Servicio que será instanciada en una interface de tipo
 * {@link IRegistroService} y hará la comunicación hacia el repository que hará
 * las transacciones a la DB a través del DAO {@link IRegistroDao}
 * 
 * @see {@link IRegistroService} {@link IRegistroDao}
 * @author Fabián Millán
 */
@Service
public class IRegistroServiceImp implements IRegistroService {

	// Se instancia el DAO a traveés de la inyección de dependencias
	@Autowired
	private IRegistroDao registroDao;

	/**
	 * Regresa todos los registros guardados en la database
	 * 
	 * @return una lista con todos los registros en la DB
	 * @see {@link Registro}
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Registro> findAll() {
		return (List<Registro>) registroDao.findAll();
	}

	/**
	 * Guarda un registro en la DB y lo regresa (Util para obtener el id)
	 * 
	 * @return el registro guardado
	 * @param registro el registro a persistir
	 * @see {@link Registro}
	 */
	@Override
	@Transactional
	public void update(Registro registro) {
		registroDao.save(registro);
	}

	/**
	 * Recibe un objeto de tipo Registro y es almacenado en la DB
	 * 
	 * @param registro objeto de tipo Registro a ser actualizado
	 * @see {@link Registro}
	 */
	@Override
	public Registro save(Registro registro) {
		return registroDao.save(registro);
	}

	/**
	 * Busca un objeto en la DB y lo regresa por su ID
	 * 
	 * @see {@link Registro}
	 * @param id el id único del objeto a buscar en la DB
	 */
	@Override
	public Registro findById(Long id) {
		return registroDao.findById(id).orElse(null);
	}

}
