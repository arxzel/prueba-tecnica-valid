package com.valid.pruebapractica.dao;

import org.apache.catalina.startup.ClassLoaderFactory.Repository;
import org.springframework.data.repository.CrudRepository;

import com.valid.pruebapractica.entity.Registro;

/**
 * 
 * Interface extiende de CrudRepository del core de JPA para Hibernate de Spring
 * para controlar la trasacciones a la DB de los objetos a través del ORM
 * 
 * @see {@link CrudRepository} {@link Repository}
 * @author Fabián Millán
 * 
 */
public interface IRegistroDao extends CrudRepository<Registro, Long> {

}
