package com.valid.pruebapractica.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Pojo que es mapeado como tabla a la DB Es la base principal de esta prueba
 * técnica para el control del obeto
 * 
 * @see {@link https://spring.io/guides/gs/accessing-data-jpa/}
 * @author Fabián Millán
 */
@Entity
@Table(name = "registros")
public class Registro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id // Id de la tabla
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	// Nombre del registro
	@NotNull
	@Basic(optional = false)
	@Column(nullable = false)
	private String nombre;

	// Apellido del registro
	@NotNull
	@Basic(optional = false)
	@Column(nullable = false)
	private String apellido;

	// El estado procesado que será cambiado en este ejercicio
	@NotNull
	@Basic(optional = false)
	@Column(nullable = false)
	private boolean procesado;

	public Registro() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public boolean isProcesado() {
		return procesado;
	}

	public void setProcesado(boolean procesado) {
		this.procesado = procesado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Registro other = (Registro) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Registro [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", procesado=" + procesado
				+ "]";
	}

}
